package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Optional;

import com.example.demo.entity.TodoItem;
import com.example.demo.repository.TodoItemRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TodoItemRepositoryTest {

    @Autowired
    private TodoItemRepository repository;

    @Test
    public void testSaveAndRetrieveTodoItem() {
        // Save a TodoItem
        TodoItem todoItem = new TodoItem();
        todoItem.setTitle("Test Todo");
        todoItem.setDescription("Test description");
        TodoItem savedItem = repository.save(todoItem);

        // Retrieve it
        Optional<TodoItem> retrievedItem = repository.findById(savedItem.getId());
        assertThat(retrievedItem).isPresent();
        assertThat(retrievedItem.get().getTitle()).isEqualTo("Test Todo");
    }

    @Test
    public void testFindAll() {
        // Save some TodoItems
        TodoItem todoItem1 = new TodoItem();
        todoItem1.setTitle("Todo 1");
        TodoItem todoItem2 = new TodoItem();
        todoItem2.setTitle("Todo 2");

        repository.save(todoItem1);
        repository.save(todoItem2);

        // Retrieve all TodoItems
        List<TodoItem> todoItems = repository.findAll();
        assertThat(todoItems).hasSizeGreaterThan(1);
    }

    @Test
    public void testUpdateTodoItem() {
        // Save a TodoItem
        TodoItem todoItem = new TodoItem();
        todoItem.setTitle("Update Me");
        repository.save(todoItem);

        // Update it
        todoItem.setTitle("Updated Todo");
        repository.save(todoItem);

        // Retrieve and verify the updated TodoItem
        Optional<TodoItem> updatedItem = repository.findById(todoItem.getId());
        assertThat(updatedItem).isPresent();
        assertThat(updatedItem.get().getTitle()).isEqualTo("Updated Todo");
    }

    @Test
    public void testDeleteTodoItem() {
        // Save a TodoItem
        TodoItem todoItem = new TodoItem();
        todoItem.setTitle("Delete Me");
        repository.save(todoItem);

        // Delete it
        repository.deleteById(todoItem.getId());

        // Verify it's no longer in the repository
        Optional<TodoItem> deletedItem = repository.findById(todoItem.getId());
        assertThat(deletedItem).isEmpty();
    }
}
