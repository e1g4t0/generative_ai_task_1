package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import java.util.Optional;

import com.example.demo.entity.TodoItem;
import com.example.demo.repository.TodoItemRepository;
import com.example.demo.service.TodoItemService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TodoItemServiceTest {

    @Mock
    private TodoItemRepository repository;

    @InjectMocks
    private TodoItemService service;

    @Test
    public void testGetTodoItemById() {
        // Create a sample TodoItem
        TodoItem todoItem = new TodoItem();
        todoItem.setId(1L);
        todoItem.setTitle("Test Todo");
        todoItem.setDescription("Test description");

        // Mock the repository's findById method
        when(repository.findById(1L)).thenReturn(Optional.of(todoItem));

        // Retrieve the TodoItem
        Optional<TodoItem> retrievedItem = service.getTodoItemById(1L);
        assertThat(retrievedItem).isPresent();
        assertThat(retrievedItem.get().getTitle()).isEqualTo("Test Todo");
    }

    @Test
    public void testUpdateTodoItem() {
        // Create a sample TodoItem
        TodoItem todoItem = new TodoItem();
        todoItem.setId(1L);
        todoItem.setTitle("Update Me");

        // Mock the repository's existsById and save methods
        when(repository.existsById(1L)).thenReturn(true);
        when(repository.save(any(TodoItem.class))).thenReturn(todoItem);

        // Update the TodoItem
        TodoItem updatedItem = service.updateTodoItem(1L, todoItem);
        assertThat(updatedItem).isNotNull();
        assertThat(updatedItem.getTitle()).isEqualTo("Update Me");
    }
}
