package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.TodoItem;
import com.example.demo.repository.TodoItemRepository;
import java.util.List;
import java.util.Optional;

@Service
public class TodoItemService {
    private final TodoItemRepository repository;

    @Autowired
    public TodoItemService(TodoItemRepository repository) {
        this.repository = repository;
    }

    public List<TodoItem> getAllTodoItems() {
        return repository.findAll();
    }

    public Optional<TodoItem> getTodoItemById(Long id) {
        return repository.findById(id);
    }

    public TodoItem createTodoItem(TodoItem todoItem) {
        return repository.save(todoItem);
    }

    public TodoItem updateTodoItem(Long id, TodoItem todoItem) {
        if (repository.existsById(id)) {
            todoItem.setId(id);
            return repository.save(todoItem);
        }
        return null; // Handle not found error
    }

    public void deleteTodoItem(Long id) {
        repository.deleteById(id);
    }
}

